import { Controller, Get, Param, Post, Query } from '@nestjs/common';
import { docDownloadBase64, downloadDocument } from 'src/helper/document-download.helper';
import { DocumentsService } from './documents.service';

@Controller('document')
export class DocumentsController {

    constructor(private docService: DocumentsService) { }

    // GENERATE A WORD DOCUMENT
    @Get('generate')
    async generateDocument() {
        //call to service method to create the document
        //The param to the createDoc method can be taken by the 
        // incoming request as request body or req params
        return this.docService.createDoc('demo-name', 'demo-data')
    }

    // DOWNLOAD ANY FILE WITH URL
    @Get('download')
    async downloadDocument(@Query('url') url: string) {
        // call to helper method to download the document
        // The param to the downloadDocument method can be taken by the
        // incoming request as request body or req params
        return await downloadDocument(url)
    }

    // DOWNLOAD FILE AND ATTACH IN EMAIL USING BASE64 FORMATTING
    @Get('downloadBase64')
    async downloadDocumentBase64(@Query('url') url: string) {
        // call to helper method to download the document
        // The param to the downloadDocument method can be taken by the
        // incoming request as request body or req params
        return await docDownloadBase64(url)
    }

}
