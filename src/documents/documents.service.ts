import { Injectable } from '@nestjs/common';
import { Packer } from 'docx';
import { writeFileSync } from 'fs';
import { createNewDoc } from 'src/helper/document.helper';

@Injectable()
export class DocumentsService {
    constructor() { }

    createDoc(name: string, data: string) {
        // call to helper method
        // this returns a document object created by docx lib
        let doc = createNewDoc(name, data);

        // Used to export the file into a .docx file
        Packer.toBuffer(doc).then((buffer) => {
            writeFileSync('My Document.docx', buffer);
        });
    }
}
